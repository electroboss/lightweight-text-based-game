#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#ifdef _WIN32
#include "files/filesWin/winkeypress.c"
#include "files/filesWin/ansi_escapes.c"
#else
#include "files/filesUNIX/kbhit.c"
#endif

#include "files/main.h"

#include "files/save.h"
#include "files/2Player.h"
#include "files/quit.h"

unsigned char main(int argc, char* argv[])
{
#ifdef _WIN32
  setupConsole();
#endif
  p2 = argc;
  printf("\033[?25l");
  signal(SIGINT, INThandler);
  for (unsigned char i = 0;i <= LHEIGHT + BONUSLINES;i++)
  {
    printf("\n");
  }
  unsigned char x;

  while (1)
  {
    if (p2 >= 2)
    {
      send1pPos(mappos(1), level);
      if (get2pPos()[1] > level & get2pPos()[1] < LEVELS) {
        level = get2pPos()[1];
      }
    }
    if (STOP)
    {
      return STOPCODE;
    }

    updatemap();
    draw();

    if (kbhit())
    {
#ifdef _WIN32
      x = getKeyPress();
#else
      x = getchar();
#endif

      switch (x)
      {
      case 'w':
        pos[1]--;
        if (colmap[level][mappos(0)] == 'r')
        {
          pos[1]++;
        }
        break;

      case 'a':
        pos[0]--;
        if (colmap[level][mappos(0)] == 'r')
        {
          pos[0]++;
        }
        break;

      case 's':
        pos[1]++;
        if (colmap[level][mappos(0)] == 'r')
        {
          pos[1]--;
        }
        break;

      case 'd':
        pos[0]++;
        if (colmap[level][mappos(0)] == 'r')
        {
          pos[0]--;
        }
        break;

      case 'r': // SEECRET
        SEECRET = 128;
        break;

      case 'q':
        save(pos[0], pos[1], level);
        break;

      case 'e':
        pos[0] = load(0);
        pos[1] = load(1);
        level = load(2);
        break;

      default:
        break;
      }

#ifdef _WIN32
      Sleep(40);
#else
      usleep(40000);
#endif
    }

    currentColour = colmap[level][mappos(0)];
    currentTile = basemap[level][mappos(0)];

    if (currentColour == 'b' && currentTile == '&')
    {
      level++;
      coins = 1;
      pos[0] = defpos[0];
      pos[1] = defpos[1];
      if (level >= LEVELS)
      {
        gameCompleted();
      }
    }
    if (currentColour == 'R' && currentTile == '%')
    {
      coins--;
      if (coins <= 1)
      {
        coins = 1;
        gameOver();
      }
    }
    if (currentColour == 'y' && currentTile == '0')
    {
      coins++;
    }
    if (currentColour == 'y' && currentTile == '1')
    {
      coins += 0.02;
    }

    fflush(stdout);
#ifdef _WIN32
    Sleep(20);
#else
    usleep(20 * 1000);
#endif
  }

  return 0;
}

unsigned char draw(void)
{
  printf("\r");
  for (unsigned char i = 0;i < LHEIGHT + BONUSLINES;i++)
  {
    printf("\033[A");
  }
  printf("%s\033[0m           \n", map);
  if (SEECRET)
  {
    printf("Riley is the best! ");
#ifdef _WIN32
    printf("\x009c");
#else
    printf("\u00A3");
#endif
    printf("%.2f  \n\033[0;30m", coins - 1);
    SEECRET -= 1;
  }
  else
  {
    printf("Coins: ");
#ifdef _WIN32
    printf("\x009c");
#else
    printf("\u00A3");
#endif
    printf("%.2f                           \n\033[0;30m", coins - 1);
  }

  return 0;
}

unsigned char updatemap(void)
{
  unsigned short j = 0;
  for (unsigned char i = 0;i <= LHEIGHT * LWIDTH;i++)
  {
    map2[i + j] = basemap[level][i];
    colmap2[i + j] = colmap[level][i];
    if (i % 8 == 7)
    {
      j++;
      map2[i + j] = '\n';
      colmap2[i + j] = '\n';
    }
  }

  map2[mappos(1)] = '@';
  colmap2[mappos(1)] = ' ';
  if (p2 >= 2)
  {
    map2[get2pPos()[0]] = '@';
    colmap2[get2pPos()[0]] = 'g';
  }

  j = 0;
  for (unsigned char i = 0;i <= LHEIGHT * (LWIDTH + 1);i++)
  {
#ifdef _WIN32
    switch (colmap2[i])
    {
    case 'r':
      map[j] = '\033';j++;
      map[j] = '[';j++;
      map[j] = '0';j++;
      map[j] = ';';j++;
      map[j] = '3';j++;
      map[j] = '1';j++;
      map[j] = 'm';j++;
      break;

    case 'g':
      map[j] = '\033';j++;
      map[j] = '[';j++;
      map[j] = '0';j++;
      map[j] = ';';j++;
      map[j] = '3';j++;
      map[j] = '2';j++;
      map[j] = 'm';j++;
      break;

    case 'R':
      map[j] = '\033';j++;
      map[j] = '[';j++;
      map[j] = '1';j++;
      map[j] = ';';j++;
      map[j] = '3';j++;
      map[j] = '1';j++;
      map[j] = 'm';j++;
      break;

    case 'y':
      map[j] = '\033';j++;
      map[j] = '[';j++;
      map[j] = '0';j++;
      map[j] = ';';j++;
      map[j] = '3';j++;
      map[j] = '3';j++;
      map[j] = 'm';j++;
      break;

    case 'b':
      map[j] = '\033';j++;
      map[j] = '[';j++;
      map[j] = '0';j++;
      map[j] = ';';j++;
      map[j] = '9';j++;
      map[j] = '4';j++;
      map[j] = 'm';j++;
      break;

    default:
      map[j] = '\033';j++;
      map[j] = '[';j++;
      map[j] = '0';j++;
      map[j] = 'm';j++;
      break;
    }
#else
    map[j] = '\033';j++;
    map[j] = '[';j++;
    map[j] = '3';j++;
    map[j] = '8';j++;
    map[j] = ';';j++;
    map[j] = '2';j++;
    map[j] = ';';j++;
    switch (colmap2[i])
    {
    case 'r': //#ff0000 255,000,000
      map[j] = '2';j++;
      map[j] = '5';j++;
      map[j] = '5';j++;
      map[j] = ';';j++;
      map[j] = '0';j++;
      map[j] = ';';j++;
      map[j] = '0';j++;
      map[j] = 'm';j++;
      break;

    case 'g': //#00ff00 000,255,000
      map[j] = '0';j++;
      map[j] = ';';j++;
      map[j] = '2';j++;
      map[j] = '5';j++;
      map[j] = '5';j++;
      map[j] = ';';j++;
      map[j] = '0';j++;
      map[j] = 'm';j++;
      break;

    case 'R': //#ff5555 255,085,085
      map[j] = '2';j++;
      map[j] = '5';j++;
      map[j] = '5';j++;
      map[j] = ';';j++;
      map[j] = '8';j++;
      map[j] = '5';j++;
      map[j] = ';';j++;
      map[j] = '8';j++;
      map[j] = '5';j++;
      map[j] = 'm';j++;
      break;

    case 'y': //#ffff00 255,000,000
      map[j] = '2';j++;
      map[j] = '5';j++;
      map[j] = '5';j++;
      map[j] = ';';j++;
      map[j] = '2';j++;
      map[j] = '5';j++;
      map[j] = '5';j++;
      map[j] = ';';j++;
      map[j] = '0';j++;
      map[j] = 'm';j++;
      break;

    case 'b': //#0000ff 000,000,255
      map[j] = '0';j++;
      map[j] = ';';j++;
      map[j] = '0';j++;
      map[j] = ';';j++;
      map[j] = '2';j++;
      map[j] = '5';j++;
      map[j] = '5';j++;
      map[j] = 'm';j++;
      break;

    default: //#ffffff 255,255,255
      map[j] = '2';j++;
      map[j] = '5';j++;
      map[j] = '5';j++;
      map[j] = ';';j++;
      map[j] = '2';j++;
      map[j] = '5';j++;
      map[j] = '5';j++;
      map[j] = ';';j++;
      map[j] = '2';j++;
      map[j] = '5';j++;
      map[j] = '5';j++;
      map[j] = 'm';j++;
      break;
    }
#endif
    map[j] = map2[i];j++;
  }

  return 0;
}